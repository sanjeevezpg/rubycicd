Gem::Specification.new do |s|
  s.name        = 'san139_hello_world'
  s.version     = '3.0.3'
  s.summary     = 'hello world example for testing gitlab CI/CD Job'
  s.description = 'hello world example for testing gitlab CI/CD Job'
  s.email       = 'sanjeev.kumar@intimetec.com'
  s.authors     = ['Sanj']
  s.homepage    = 'http://rubygems.org/gems/san139_hello_world'
  s.license     = 'MIT'
  s.required_rubygems_version = '>= 1.3.5'
  s.required_ruby_version = '>= 2.3'
  s.add_dependency 'rest-client', '>= 1.8.0', '< 3.0.0'
  s.add_development_dependency 'rspec', '~> 0'
end
  