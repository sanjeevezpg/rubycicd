# The `never` will enforce that the frozen string literal comment does
# not exist in a file.
# bad
# frozen_string_literal: true

def print_hello
  x = 1
  name = 'John'
  if x.zero?
    puts 'Bie'
  else
    puts 'Hello ' + name
  end
end
print_hello