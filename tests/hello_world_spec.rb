describe "An examle to test ruby hello word" do
    it "should show how the equality matches work" do
        a = "Hello"
        b = a
        expect(a).to eq "Hello"
        expect(a).to eql "Hello"
    end
end
